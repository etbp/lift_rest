name := "todo lift"

version := "0.0.1"

organization := "ca.polymtl"

scalaVersion := "2.9.1"

seq( webSettings :_* )

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
	val liftVersion = "2.4"
	Seq(
		"net.liftweb"					%% "lift-webkit"		    % liftVersion			% "compile",
		"org.eclipse.jetty"	 		    %  "jetty-webapp"			% "7.5.4.v20111024"	    % "container; test",
		"ch.qos.logback"                %  "logback-classic"        % "1.0.6",
		"org.scalatest"				    %% "scalatest"				% "2.0.M3"				% "test",
		"com.foursquare"      	        %% "rogue"           	    % "1.1.8"
  	)
}

port in container.Configuration := 8080

parallelExecution in Test := false