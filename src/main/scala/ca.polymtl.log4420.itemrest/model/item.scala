package ca.polymtl.log4420.itemrest
package model

import lib.RestMongo

// Lift
import net.liftweb._
import common.{Full, Failure, Box}
import common.Box._

// Lift Json
import json._
import json.JsonAST.JValue

// Lift DB
import mongodb.record._
import field.MongoListField
import record.field._

// Rogue
import com.foursquare.rogue.Rogue._

// Bson
import org.bson.types.ObjectId

class Item extends MongoRecord[Item] with MongoId[Item]
{
  def meta = Item

  object name extends StringField(this, 255)
  object keywords extends MongoListField[Item, String](this)
  object price extends DecimalField(this,0)
  object taxable extends BooleanField(this)
  object weightInGrams extends LongField(this)
  object quantity extends LongField(this)

  implicit def toJson(item : Item): JValue = item.asJValue
  implicit def toJsonFromSeq(item : Seq[Item]): JValue = JArray( item.map( c => (c.asJValue): JValue ).toList )
  implicit def toJsonFromOption( item: Option[Item] ): Option[JValue] = item.map( _.asJValue )
  implicit def toJsonFromBox( item: Box[Item] ): Box[JValue] = item.map(_.asJValue )
}

object Item extends Item with MongoMetaRecord[Item]
{
  override def mongoIdentifier = RestMongo

  def createFromJson( json: JValue ) =
  {
    val item = Item.createRecord
    item.setFieldsFromJValue( json ).map( _ => item.save )
  }

  def findById( id: String ): Box[Item] =
  {
    for {
      oid <- objectId( id )
      item <- byId( oid ) get()
    } yield item
  }

  def deleteById( id: String ): Box[Item] =
  {
    for {
      oid <- objectId( id )
      item <- byId( oid ) findAndDeleteOne()
    } yield item
  }

  def findByKeywords( keywords: List[String ] ) =
  {
    Item where ( _.keywords in keywords ) fetch()
  }

  def updateById( id: String, json: JValue ): Box[Item] =
  {
    val updatedItem =
    for {
      oid <- objectId( id )
      item <- byId( oid ) get()
      _ <- item.setFieldsFromJValue( json )
    } yield item

    updatedItem.map( _.save )
  }: Box[Item]

  private def objectId( id: String ) =
  {
    try{ Full( new ObjectId( id ) ) }
    catch { case e: IllegalArgumentException => Failure( e.toString ) }
  }

  private def byId( oid: ObjectId ) = Item where ( _._id eqs oid )
}