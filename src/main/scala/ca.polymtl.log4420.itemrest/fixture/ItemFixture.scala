package ca.polymtl.log4420.itemrest
package fixture

import model.Item

object ItemFixture
{
  def insertIfEmpty()
  {
    if( Item.count == 0 )
    {
      Item.createRecord
        .name( "Cute paws' food")
        .keywords( List("Cat", "Food") )
        .price( 4.25 )
        .taxable( true )
        .weightInGrams( 1000 )
        .quantity( 20 )
        .save

      Item.createRecord
        .name( "Wouaf Wouaf")
        .keywords( List("Dog", "Food") )
        .price( 7.25 )
        .taxable( true )
        .weightInGrams( 5000 )
        .quantity( 70 )
        .save

      Item.createRecord
        .name( "Bubbles")
        .keywords( List("Fish", "Food") )
        .price( 1.25 )
        .taxable( true )
        .weightInGrams( 300 )
        .quantity( 40 )
        .save

      Item.createRecord
        .name( "Birdy")
        .keywords( List("Bird", "Food") )
        .price( 5.25 )
        .taxable( true )
        .weightInGrams( 800 )
        .quantity( 10 )
        .save

    }
  }
}