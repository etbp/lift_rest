package ca.polymtl.log4420.itemrest.lib

import net.liftweb.mongodb.{MongoDB, MongoIdentifier}
import com.mongodb.{Mongo, ServerAddress}

object RestMongo extends MongoIdentifier
{
  override def jndiName = "log4420_mongo"

  private var mongo: Option[Mongo] = None

  def start()
  {
    mongo = Some( new Mongo( new ServerAddress( "localhost", 27017 ) ) )
    MongoDB.defineDb( RestMongo, mongo.get, "log4420-rest" )
  }

  def stop()
  {
    for { db <- mongo }
    {
      db.close()
      MongoDB.close
      mongo = None
    }
  }
}
